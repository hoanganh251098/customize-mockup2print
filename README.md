# README #

### What is this repository for? ###

Chuyển đổi file preview của customize qua file in ở một số sản phẩm customize đặc biệt

### Details ###

Một số sản phẩm customize như ornament, áo hawaii.

Các sản phẩm này thường chứa 2 sản phẩm customize con:

* Một sản phẩm để hiển thị cho khách xem giống như mockup
* Sản phẩm còn lại để ghép các input từ sản phẩm trên tạo thành file in

Tool nhận input là preview url của sản phẩm (1) sau đó tạo file export của sản phẩm (2)

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

[node.js]: <http://nodejs.org>