const Vue = require('vue/dist/vue.min.js');
const Vuex = require('vuex');
const ElementUI = require('element-ui');
const { Loading } = require('element-ui');
const puppeteer = require('puppeteer');
const axios = require('axios');
const fs = require('fs');
const clipboardy = require('clipboardy');

Vue.use(Vuex);
Vue.use(ElementUI);

const App = new Vue({
    mounted() {
    },
    data: () => ({
        demoText: '',
        styles: {
            '--background-color': '#eee',
            '--text-color': '#666'
        },
        inputSKU: '',
        inputImageURL: 'https://tat-customize.s3-accelerate.amazonaws.com/51656836268286274158142407473345735558.png',
        outputImageURL: '',
        progress: 0,
        headless: true
    }),
    computed: {
        
    },
    methods: {
        onInputImageURL() {
            this.inputSKU = '';
        },
        copyOutputImageURL() {
            clipboardy.writeSync(this.outputImageURL);
            alert('Copied');
        },
        async convert() {
            this.inputSKU = '';
            let hashCode;
            // ----------------------------------
            this.progress = 0;
            try {
                hashCode = this.inputImageURL.match(/(?<=amazonaws\.com\/)[a-zA-Z0-9]+/g)[0];
            }
            catch(e) {
                alert('Failed to get hash code.');
                this.progress = 0;
                return;
            }
            // ----------------------------------
            this.progress = 10;
            const orderData = await new Promise((resolve, reject) => {
                axios.get(`http://dev.navitee.com/design-upload/export_synth/${hashCode}.json`)
                .then(res => {
                    resolve(res.data);
                })
                .catch(err => {
                    reject(err);
                });
            });
            // ----------------------------------
            this.progress = 20;
            const sku = orderData.productName;
            this.inputSKU = sku;
            const scriptName = sku + '.js';
            const mappingScriptNames = fs.readdirSync('mapping');
            if(!mappingScriptNames.includes(scriptName)) {
                alert('Mockup2print mapping not found.');
                this.progress = 0;
                return;
            }
            const script = require('./mapping/' + scriptName);
            let result = script.execute(orderData);
            if(result instanceof Promise) {
                result = await result;
            }
            const printSKU = result.productName;
            const printProductURL = `https://dev.navitee.com/iframe/dist/?product=${printSKU}&mode=debug`;
            const browser = await puppeteer.launch({ headless: this.headless, defaultViewport: null, });
            const page = await browser.newPage();
            // ----------------------------------
            this.progress = 30;
            await page.goto(printProductURL, { waitUntil: 'networkidle2' });
            // ----------------------------------
            this.progress = 40;
            await page.evaluate(async (_) => {
                await App.canvas.import(_);
            }, result);
            // ----------------------------------
            this.progress = 80;
            const r = await page.evaluate(async (_) => {
                return await App.helper.uploadBlobAWS(App.helper.toBlob(await App.canvas.exportBase64()));
            });
            await browser.close();
            // ----------------------------------
            this.progress = 100;
            this.outputImageURL = r.data;
        }
    }
}).$mount(document.querySelector('#app'));

