const fs = require('fs');

function execute(orderData) {
    const frontMap = {
        3: 2,
        4: 3,
        5: 4,
        7: 5,

        9: 9,
        10: 8,
        11: 7,
        13: 6,

        15: 10,
        16: 11,
        17: 12,
        19: 13,

        21: 14,
        22: 15,
        23: 16,
        25: 17,

        27: 18,
        28: 19,
        29: 25,
        31: 24,

        39: 20,
        40: 21,
        41: 22,
        43: 23,

        33: 2,
        34: 3,
        35: 4,
        37: 5,
    };
    frontMap[6] = frontMap[33];
    frontMap[12] = frontMap[39];
    frontMap[18] = frontMap[27];
    frontMap[24] = frontMap[21];
    frontMap[30] = frontMap[15];
    frontMap[42] = frontMap[9];
    frontMap[36] = frontMap[3];

    frontMap[2] = frontMap[37];
    frontMap[8] = frontMap[43];
    frontMap[14] = frontMap[31];
    frontMap[20] = frontMap[25];
    frontMap[26] = frontMap[19];
    frontMap[38] = frontMap[13];
    frontMap[32] = frontMap[7];

    const backMap = {
        61: 2,
        62: 3,
        63: 4,
        65: 5,

        67: 9,
        68: 8,
        69: 7,
        71: 6,

        73: 10,
        74: 11,
        75: 12,
        77: 13,

        79: 14,
        80: 15,
        81: 16,
        83: 17,

        85: 18,
        86: 19,
        87: 25,
        89: 24,

        97: 20,
        98: 21,
        99: 22,
        101: 23,

        91: 2,
        92: 3,
        93: 4,
        95: 5
    };
    backMap[64] = frontMap[33];
    backMap[70] = frontMap[39];
    backMap[76] = frontMap[27];
    backMap[82] = frontMap[21];
    backMap[88] = frontMap[15];
    backMap[100] = frontMap[9];
    backMap[94] = frontMap[3];
    
    backMap[60] = frontMap[37];
    backMap[66] = frontMap[43];
    backMap[72] = frontMap[31];
    backMap[78] = frontMap[25];
    backMap[84] = frontMap[19];
    backMap[96] = frontMap[13];
    backMap[90] = frontMap[7];

    const leftSleeveMap = {
        55: 26,
        54: 27,
        59: 29,
        58: 28,
        53: 26,
        52: 27,
        57: 29,
        56: 28
    };
    const rightSleeveMap = {
        47: 32,
        46: 30,
        51: 33,
        50: 31,
        45: 32,
        44: 30,
        49: 33,
        48: 31
    };
    let jsonOutput = fs.readFileSync('./mapping/assets/hawaiit-pet-120721-photo-custom.json');
    jsonOutput = JSON.parse(jsonOutput);
    for(const [outputIdx, inputIdx] of Object.entries({
        ...frontMap,
        ...backMap,
        ...leftSleeveMap,
        ...rightSleeveMap
    })) {
        jsonOutput.layers[outputIdx].userInput.image = orderData.layers[inputIdx].userInput.image;
    }
    return jsonOutput;
}

module.exports = { execute };
