function execute(orderData) {
    return {
        productName: "hawaiit-dog-260621-summer-photo-custom",
        timestamp: orderData.timestamp,
        layers: [
            orderData.layers[0],
            orderData.layers[1],
            orderData.layers[2],
            orderData.layers[3],
            orderData.layers[4],
            orderData.layers[5],
            orderData.layers[2],
            orderData.layers[3],
            orderData.layers[4],
            orderData.layers[5],
            orderData.layers[6]
        ]
    }
}

module.exports = { execute };